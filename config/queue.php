<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Connection Name
    |--------------------------------------------------------------------------
    |
    | Laravel's queue API supports an assortment of back-ends via a single
    | API, giving you convenient access to each back-end using the same
    | syntax for every one. Here you may define a default connection.
    |
    */

    'default' => env('QUEUE_CONNECTION', 'sync'),

    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    | Drivers: "sync", "database", "beanstalkd", "sqs", "redis", "null"
    |
    */

    'connections' => [

        'sync' => [
            'driver' => 'sync',
        ],

        'database' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'default',
            'retry_after' => 7200,
        ],
        
        'queue_one' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_one',
            'retry_after' => 7200,
        ],
        'queue_two' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_two',
            'retry_after' => 7200,
        ],
        'queue_three' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_three',
            'retry_after' => 7200,
        ],
        'queue_four' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_four',
            'retry_after' => 7200,
        ],
        'queue_five' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_five',
            'retry_after' => 7200,
        ],
        'queue_six' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_six',
            'retry_after' => 7200,
        ],
        'queue_seven' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_seven',
            'retry_after' => 7200,
        ],
        'queue_eight' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_eight',
            'retry_after' => 7200,
        ],
        'queue_nine' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_nine',
            'retry_after' => 7200,
        ],
        'queue_ten' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_ten',
            'retry_after' => 7200,
        ],
        'queue_new' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new',
            'retry_after' => 7200,
        ],
        'queue_new_one' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_one',
            'retry_after' => 7200,
        ],
        'queue_new_two' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_two',
            'retry_after' => 7200,
        ],
        'queue_new_three' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_three',
            'retry_after' => 7200,
        ],
        'queue_new_four' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_four',
            'retry_after' => 7200,
        ],
        'queue_new_five' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_five',
            'retry_after' => 7200,
        ],
        'queue_new_six' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_six',
            'retry_after' => 7200,
        ],
        'queue_new_seven' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_seven',
            'retry_after' => 7200,
        ],
        'queue_new_eight' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_eight',
            'retry_after' => 7200,
        ],
        'queue_new_nine' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_nine',
            'retry_after' => 7200,
        ],
        'queue_new_ten' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_ten',
            'retry_after' => 7200,
        ],
        
        'queue_new_friday' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'queue_new_friday',
            'retry_after' => 7200,
        ],

        'beanstalkd' => [
            'driver' => 'beanstalkd',
            'host' => 'localhost',
            'queue' => 'default',
            'retry_after' => 90,
            'block_for' => 0,
        ],

        'sqs' => [
            'driver' => 'sqs',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'prefix' => env('SQS_PREFIX', 'https://sqs.us-east-1.amazonaws.com/your-account-id'),
            'queue' => env('SQS_QUEUE', 'your-queue-name'),
            'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
        ],

        'redis' => [
            'driver' => 'redis',
            'connection' => 'default',
            'queue' => env('REDIS_QUEUE', 'default'),
            'retry_after' => 1800,
            'block_for' => null,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control which database and table are used to store the jobs that
    | have failed. You may change them to any database / table you wish.
    |
    */

    'failed' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table' => 'failed_jobs',
    ],

];
