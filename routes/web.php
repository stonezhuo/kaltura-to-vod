<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Item;
use App\KalturaVideo;
use App\Imports\ItemsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

Route::get('/', function () {
    return view('welcome');
});

Route::post('/user-data-notify', function () {
    $callback_str = file_get_contents('php://input');
    \Log::info('user-data-notify:'.$callback_str);
    $callback_obj = json_decode($callback_str);
    $extend_obj = json_decode($callback_obj->Extend);

    KalturaVideo::where('guid', $extend_obj->guid)->update(['ali_file_url' => $callback_obj->FileUrl, 'ali_video_id' => $callback_obj->VideoId,'status' => 3]);
});
Route::post('/ali-vod-upload-video-notify', function () {
    \Log::info('ali-vod-upload-video-notify:'.json_encode(request()->all()));
});

Route::get('/upload-video', function () {
    
});

Route::get('/read-csv', function () {
    /* $file_list = glob(public_path('csvs').'/*');
    foreach ($file_list as $key => $file_name) {
        Excel::import(new ItemsImport, $file_name);
    } */
    Excel::import(new ItemsImport, public_path('Items.csv'));
});

Route::post('/item-notify', function (Request $request) {
    $callback_parameters = $request->getContent();
    \Log::info('[item-notify][callback_parameters]:'.$callback_parameters);
    $callback_data = json_decode($callback_parameters, true);
    $extend_data = json_decode($callback_data['Extend'], true);

    if ('UploadByURLComplete' == $callback_data['EventType']) {
        if ('success' == $callback_data['Status']) {
            Item::where('guid', $extend_data['guid'])->update([
                'ali_source_url' => $callback_data['SourceURL'],
                'ali_video_id' => $callback_data['VideoId'],
                'ali_size' => $callback_data['Size'],
                'status' => 1,
            ]);
        } else {
            Item::where('guid', $extend_data['guid'])->update([
                'status' => 4,
            ]);
        }
    } else if ('FileUploadComplete' == $callback_data['EventType']) {
        Item::where('guid', $extend_data['guid'])->update([
            'ali_file_url' => $callback_data['FileUrl'],
            'ali_video_id' => $callback_data['VideoId'],
            'ali_size' => $callback_data['Size'],
            'status' => 2,
        ]);
    } else if ('TranscodeComplete' == $callback_data['EventType']) {
        if ('success' == $callback_data['Status']) {
            Item::where('guid', $extend_data['guid'])->update([
                'ali_video_id' => $callback_data['VideoId'],
                'status' => 3,
            ]);
        } else {
            Item::where('guid', $extend_data['guid'])->update([
                'status' => 5,
            ]);
        }
    }
});