<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKalturaVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kaltura_videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('guid')->unique()->default('');
            $table->string('title')->default('');
            $table->string('description')->default('');
            $table->string('category')->default('');
            $table->string('video_url')->default('');
            $table->string('type')->default('');
            $table->string('thumbnail_url')->default('');
            $table->unsignedSmallInteger('thumbnail_with')->default(0);
            $table->unsignedSmallInteger('thumbnail_height')->default(0);
            $table->string('ali_file_url')->default('');
            $table->string('ali_video_id')->default('');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kaltura_videos');
    }
}
