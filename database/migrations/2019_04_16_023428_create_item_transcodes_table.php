<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTranscodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_transcodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('item_id')->nullable()->index();
            $table->bigInteger('size')->unsigned()->nullable();
            $table->char('definition', 2)->nullable();
            $table->unsignedTinyInteger('fps')->nullable();
            $table->float('duration', 8, 2)->nullable();
            $table->unsignedInteger('bitrate')->nullable();
            $table->string('format')->nullable();
            $table->unsignedSmallInteger('height')->nullable();
            $table->unsignedSmallInteger('width')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_transcodes');
    }
}
