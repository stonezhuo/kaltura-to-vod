<?php

namespace App\Console\Commands;

use App\KalturaVideo;
use App\Jobs\UploadVideoToVod;
use Illuminate\Console\Command;

class AddNewJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add new job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $new_videos = KalturaVideo::where('id', '>', 3322)->get()->toArray();

        foreach ($new_videos as $video) {
            UploadVideoToVod::dispatch($video)->onQueue('queue_new_friday');
        }
    }
}
