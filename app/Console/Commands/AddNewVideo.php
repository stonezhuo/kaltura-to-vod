<?php

namespace App\Console\Commands;

use App\KalturaVideo;
use Illuminate\Console\Command;

class AddNewVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:video';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add new video';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mrss_xml = simplexml_load_file('http://vcloud.000607.cn/api_v3/getFeed.php?partnerId=129&feedId=0_sxusjrrq');
        
        $exists_guids = KalturaVideo::get()->pluck('guid')->toArray();

        $videos = [];
        foreach ($mrss_xml->channel->item as $item) {
            $array_item = json_decode(json_encode($item), true);

            if (in_array($array_item['guid'], $exists_guids)) {
                break;
            }

            $item_children = $item->children('media', true);
            if ($item_children) {
                if ($item_children->content && $item_children->content->attributes()) {
                    $content_attributes = json_decode(json_encode($item_children->content->attributes()),true)['@attributes'];
                } else {
                    $content_attributes = [
                        'url' => '',
                        'type' => '',
                    ];
                }
                
                if ($item_children->thumbnail && $item_children->thumbnail->attributes()) {
                    $thumbnail_attributes = json_decode(json_encode($item_children->thumbnail->attributes()),true)['@attributes'];
                } else {
                    $thumbnail_attributes = [
                        'url' => '',
                        'width' => 0,
                        'height' => 0,
                    ];    
                }
            } else {
                $thumbnail_attributes = [
                    'url' => '',
                    'width' => 0,
                    'height' => 0,
                ];
                $content_attributes = [
                    'url' => '',
                    'type' => '',
                ];
            }

            $videos[] = [
                'guid' => $array_item['guid'],
                'title' => $array_item['title'],
                'category' => $array_item['category'],
                'video_url' => $content_attributes['url'],
                'type' => $content_attributes['type'],
                'thumbnail_url' => $thumbnail_attributes['url'],
                'thumbnail_with' => $thumbnail_attributes['width'],
                'thumbnail_height' => $thumbnail_attributes['height'],
                'created_at' => date('Y-m-d H:i:s'),
            ];
        }

        if (KalturaVideo::insert($videos)) {
            echo 'save successfully';
        } else {
            echo 'save unsuccessfully';
        }
    }
}
