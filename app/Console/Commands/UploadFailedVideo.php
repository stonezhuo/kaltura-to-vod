<?php

namespace App\Console\Commands;

use App\KalturaVideo;
use App\Jobs\UploadVideoToVod;
use Illuminate\Console\Command;

class UploadFailedVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reupload:video';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'upload video which is failed to upload last time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $failed_videos = KalturaVideo::whereIn('status', [0, 2])->get()->toArray();

        foreach ($failed_videos as $video) {
            UploadVideoToVod::dispatch($video);
        }
    }
}
