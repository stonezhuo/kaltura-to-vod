<?php

namespace App\Jobs;

require_once __DIR__.'/../../public' . DIRECTORY_SEPARATOR . 'voduploadsdk' . DIRECTORY_SEPARATOR . 'Autoloader.php';

use \Log;
use \Exception;
use App\KalturaVideo;
use AliyunVodUploader;
use UploadImageRequest;
use UploadVideoRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadVideoToVod implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    private $video = [];
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $video)
    {
        $this->video = $video;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $video = $this->video;

        $uploader = new AliyunVodUploader('LTAIBcS2q7uXb1Iv', 'PAAaZTwqlfq6Xnj9LP5q4JHD4xQJ1H');

        $image_url = '';
        try {
            if ($video['thumbnail_url']) {
                $uploadImageRequest = new UploadImageRequest($video['thumbnail_url'], $video['title']);
                $uploadImageRequest->setImageType('cover');
                $uploadImageRequest->setImageExt('jpeg');
                $res = $uploader->uploadWebImage($uploadImageRequest);
                $image_url = $res['ImageURL'];
            }
        } catch (Exception $e) {
            Log::info('testUploadWebImage Failed, ErrorMessage: '.$e->getMessage());
        }

        try {
            if ($video['video_url']) {
                $uploadVideoRequest = new UploadVideoRequest($video['video_url'], $video['title']);
                if ($image_url) {
                    $uploadVideoRequest->setCoverURL($image_url);
                }
                $uploadVideoRequest->setTemplateGroupId('e526a51aea1e8992ff5688a283a378d5');
                $userData = [
                    'MessageCallback' => ['CallbackURL' => 'http://transferKaltura.ivideocloud.cn/user-data-notify'],
                    'Extend' => ['guid' => $video['guid']],
                ];
                $uploadVideoRequest->setUserData(json_encode($userData));

                $res = $uploader->uploadWebVideo($uploadVideoRequest);
                KalturaVideo::where('guid', $video['guid'])->update(['status' => 1]);
            }

        } catch (Exception $e) {
            Log::info('testUploadWebVideo Failed, ErrorMessage: '.$e->getMessage().' Location: '.$e->getFile().' '.$e->getLine().' Trace: '.$e->getTraceAsString());

            KalturaVideo::where('guid', $video['guid'])->update(['status' => 2]);
        }
    }
}
