<?php

namespace App\Imports;

use App\Item;
use Maatwebsite\Excel\Concerns\ToModel;

class ItemsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ('guid' == $row[0] && 'title' == $row[1] && 'description' == $row[2] && 'media:content' == $row[3] && 'media:thumbnail' == $row[4]) {
            
        } else {
            if (Item::where('guid', $row[0])->count()) {

            } else {
                return new Item([
                    'guid' => $row[0],
                    'title' => $row[1] ?? '',
                    'description' => $row[2] ?? '',
                    'video_url' => $row[3],
                    'thumbnail_url' => $row[4] ?? '',
                ]);
            }
        }
    }
}
